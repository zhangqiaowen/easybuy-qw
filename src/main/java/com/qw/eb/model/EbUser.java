package com.qw.eb.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class EbUser implements Serializable {

    @TableId(value = "eu_user_id",type= IdType.INPUT)
    private String euUserId;

    private String euUserName;

    private String euPassword;
    private String euSex;

    //@DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date euBirthday;


    private String euIdentityCode;


    private String euEmail;


    private String euMobile;


    private String euAddress;


    private Integer euStatus;


    private static final long serialVersionUID = 1L;


}