package com.qw.eb.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class EbProduct implements Serializable {

    @TableId
    private Integer epId;
    private String epName;
    private String epDescription;
    private BigDecimal epPrice;
    private Integer epStock;
    private Integer epcId;//商品的一级分类id
    private Integer epcChildId;//商品的二级分类的id
    private String epFileName;

    @TableField(exist = false)
    private int count;//表示购买数量


}