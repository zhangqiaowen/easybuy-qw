package com.qw.eb.service;


import com.qw.eb.model.EbProductCategory;


import java.util.List;

public interface EbProductCategoryService {

      /**
       * 1.分页查询商品信息
       * @param psize    第几页
       * @param pageSize  每页显示多少条
       * @return
       */
      List<EbProductCategory> selectList(Integer psize, Integer pageSize);
}
