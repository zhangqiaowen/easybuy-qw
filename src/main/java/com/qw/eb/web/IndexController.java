package com.qw.eb.web;

import com.qw.eb.model.EbProductCategory;
import com.qw.eb.service.EbProductCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@Controller//迎宾证

@RequestMapping("/front")//工号
public class IndexController {

    @Autowired
    private EbProductCategoryService ebProductCategoryService;

    //行为 返回首页的数据(model)和视图(view-html)
    @RequestMapping("/index")//返回前端首页的数据和视图
    public String index(Model model,
                        @RequestParam(value = "pSize",defaultValue = "1") Integer pSize,
                        @RequestParam(value = "pageSize",defaultValue = "8")Integer pageSize){
        //1.调用service层查询分页的商品信息
        List<EbProductCategory> ebProductCategoryList = ebProductCategoryService.selectList(pSize, pageSize);
        //2.调用service层查询分页的新闻信息

        //3.调用service层查询商品分类信息

        //4.将数据绑定model中  key-value进行数据绑定
        model.addAttribute("ebProductCategoryList",ebProductCategoryList);
//        model.addAttribute("newList",newList);


        //5.返回视图(html)
        return "index";//templates/index.html

    }



}
