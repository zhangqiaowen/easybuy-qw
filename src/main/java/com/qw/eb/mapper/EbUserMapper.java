package com.qw.eb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qw.eb.model.EbUser;

public interface EbUserMapper extends BaseMapper<EbUser> {
}
