package com.qw.eb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qw.eb.model.EbOrder;

public interface EbOrderMapper extends BaseMapper<EbOrder> {
}
