package com.qw.eb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qw.eb.model.EbNews;

public interface EbNewsMapper extends BaseMapper<EbNews> {
}
