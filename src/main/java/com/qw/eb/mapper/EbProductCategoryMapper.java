package com.qw.eb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qw.eb.model.EbProductCategory;

import java.util.List;

public interface EbProductCategoryMapper extends BaseMapper<EbProductCategory> {
    //1.查询出所有大类的方法
    List<EbProductCategory> selectBigCategory();
    //2.查询出所有小类的方法
    List<EbProductCategory> selectSmallCategory();
}
