package com.qw.eb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qw.eb.model.EbOrderDetail;

public interface EbOrderDetailMapper extends BaseMapper<EbOrderDetail> {
}
