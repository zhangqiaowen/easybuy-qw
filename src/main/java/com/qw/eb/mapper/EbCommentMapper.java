package com.qw.eb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qw.eb.model.EbComment;

public interface EbCommentMapper extends BaseMapper<EbComment> {
}
