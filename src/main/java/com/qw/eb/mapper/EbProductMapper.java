package com.qw.eb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qw.eb.model.EbProduct;

public interface EbProductMapper extends BaseMapper<EbProduct> {
}
