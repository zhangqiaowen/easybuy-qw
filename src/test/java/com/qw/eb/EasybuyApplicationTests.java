package com.qw.eb;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qw.eb.mapper.EbProductCategoryMapper;
import com.qw.eb.mapper.EbProductMapper;
import com.qw.eb.model.EbProductCategory;
import org.junit.jupiter.api.Test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import sun.awt.geom.AreaOp;
/**
 * orm object relationship maping
 * object(对象) :类-->对象
 * relationship(关系) :表---
 *
 * 类----->表
 * 对象----->一行数据
 *
 */

import java.math.BigDecimal;
import java.sql.Wrapper;
import java.util.List;

@RunWith(SpringRunner.class)//alt+enter
@SpringBootTest(classes = EasybuyApplication.class)
class EasybuyApplicationTests {
    /**
     * 上课表：eb_product
     * 对应实体类:EbProduct
     * 对应映射器:EbProdutc
     *
     * String s = null;
     * s.subString(); NullPointerException 空指针异常
     * */
    @Autowired
    EbProductMapper ebProductMapper;

    //测试mybatis-plus是否可以使用
    @Test
    void contextLoads() {
        //查询eb_productcategory表：select*from eb_productcategory
        List<EbProductCategory> ebProductCategories = ebProductCategoryMapper.selectList(null);
        ebProductCategories.forEach(productCategory->{
            System.out.println(productCategory);
        });
        }

      /*  //1、测试添加一个商品
        @Test //表明他是一个单元测试
    public void whenInsertSuccess() {
        EbProduct ebProduct=new EbProduct();
        //给属性赋值----看表的非空字段
            //ebProduct.setEpcId();//主键自动增长
            ebProduct.setEpName("JK小裙子");
            ebProduct.setEpPrice(new BigDecimal("99.9"));
              //ebProduct.setEpPrice(99.9);//double 浮点型（约数）   3.9====>3.9444444亿
            ebProduct.setEpStock(99);
            ebProduct.setEpcId(542);
            ebProduct.setEpFileName("1.jpg");
            //result受影响行数 >0表示成功
            int result = ebProductMapper.insert(ebProduct);
            System.out.println("受影响的行数:"+result);
            //获取自增长的主键值
            int keyValue = ebProduct.getEpcId();
            System.out.println("kevalue:"+keyValue);
        ebProductMapper.insert(ebProduct);
        }*/
        //1.1测试添加一个商品(ProductCategory)

        @Test//
    public void InsertSuccess(){
        //给属性赋值
            EbProductCategory ebProductCategory=new EbProductCategory();
            ebProductCategory.setEpcName("JK小裙子");
            ebProductCategory.setEpcId(643);
            ebProductCategory.setEpcParentId(614);
            //result受影响行数 >0表示成功
            int result = ebProductCategoryMapper.insert(ebProductCategory);
            System.out.println("受影响的行数:"+result);
            //获取自增长的主键值
            int keyValue = ebProductCategory.getEpcId();
            System.out.println("kevalue:"+keyValue);
        }
        //2.测试修改一个商品
        //2.1根据id进行修改商品 id属性加@TableId
        //如：我将商品542的商品改为图书2
    @Test
    public void whenUpdateProductCategroySuccess(){
           EbProductCategory ebProductCategroy =new EbProductCategory();
            ebProductCategroy.setEpcId(542);
            ebProductCategroy.setEpcName("图书2");

            int result = ebProductCategoryMapper.updateById(ebProductCategroy);
            System.out.println("受影响行数:" +result);
    }
        //2.2根据id修改订单时间
        //3.测试删除一个商品
        @Test
        public void whenDeleteProductCategroySuccess(){
            //result 受影响的行数>0 表示成功
            int result = ebProductCategoryMapper.deleteById(548);
            System.out.println("受影响行数:" +result);


        }
        //4.测试分页查询商品信息
    //查询商品表
        @Test
        public void whenUpdateSuccess(){
            Page<EbProductCategory> page = new Page<>(2,4);
            //null   :  根据什么条件分页， null不根据任何条件分页
            Page<EbProductCategory> productCategoryPage = ebProductCategoryMapper.selectPage( page,null );
            //从page中获取分页的商品数据
            List<EbProductCategory>productCategoryList = productCategoryPage.getRecords();
            for(EbProductCategory ebProductCategory:productCategoryList){
                System.out.println(ebProductCategory);
            }
        }

        //5.测试高级查询个商品
    //select * from eb_proudctcategroy where ep_name like "%家%" and ep_id>625
    @Test
    public void whenSelectProductCategroySuccess(){
            List<EbProductCategory> ebProductCategories = ebProductCategoryMapper.selectList(Wrappers
                    .<EbProductCategory>lambdaQuery()
                    .like(EbProductCategory::getEpcName,"%家%")
                    .gt(EbProductCategory::getEpcId,625)//great than 大于 lt 小于
            );
            ebProductCategories.forEach(productCategory->{
                System.out.println(productCategory);
            });
    }


    @Autowired
    private EbProductCategoryMapper ebProductCategoryMapper;
    //6.商品分类大类和小类
    @Test
    public void whenSelectCategorySuccess(){
        //1.查询出所有大类
        List<EbProductCategory> bigCategory = ebProductCategoryMapper.selectBigCategory();
        System.out.println("========一级分类----------");
        bigCategory.forEach(big->{
            System.out.println(big);
        });

        //2.查询出所有的小类
        System.out.println("========一级分类----------");
        List<EbProductCategory> smallCategory = ebProductCategoryMapper.selectSmallCategory();
        smallCategory.forEach(small->{
            System.out.println(small);
        });

        //3.将小类与对应的大类进行关联
        System.out.println("========一级关联二级分类----------");
        bigCategory.forEach(big->{
            System.out.println(big.getEpcName());
            //显示属于他的小类
            smallCategory.forEach(small->{
                if(big.getEpcId().equals(small.getEpcParentId())){
                    System.out.println("\t"+small.getEpcName());
                }

            });
        });
    }




}















